package com.example.fragmentsdemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;


public class GamesFragment extends ListFragment implements AdapterView.OnItemClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_games,parent,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        ArrayList<String> games = new ArrayList<>();
        games.add("Dragon ball heroes");
        games.add("Metal Gear Solid V");
        games.add("Chrono Trigger");
        ArrayAdapter <String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,games);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
        Toast.makeText(getActivity(), "Je drukte op game " + position, Toast.LENGTH_SHORT).show();
    }
}
