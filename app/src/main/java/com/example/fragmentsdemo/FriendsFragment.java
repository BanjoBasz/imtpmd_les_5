package com.example.fragmentsdemo;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;


public class FriendsFragment extends ListFragment implements AdapterView.OnItemClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_friends,parent,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        ArrayList<String> friends = new ArrayList<>();
        friends.add("Jeroen");
        friends.add("Fred");
        friends.add("Vincent");
        ArrayAdapter <String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,friends);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
        Toast.makeText(getActivity(), "Je hebt geklikt op: " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
    }
}
